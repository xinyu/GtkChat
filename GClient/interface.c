/***********************************************
* interface for Gtk+ MultiPerson Chat
************************************************/

#include "interface.h"

void initGUI(ChatData* chatData)
{	
	chatData->accel_group = gtk_accel_group_new ();
	
	/*** Create the new window ***/
	chatData->window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	gtk_window_set_title (GTK_WINDOW (chatData->window), "MultiPerson Chat (Client)");
	gtk_window_set_default_size (GTK_WINDOW (chatData->window), 350, 400);
	
	chatData->window_icon_pixbuf = create_pixbuf ("../pixmaps/GClient.gif");
  	gtk_window_set_icon (GTK_WINDOW (chatData->window), chatData->window_icon_pixbuf);

	/*** vbox - for packing everything ***/
	chatData->vbox_whole = gtk_vbox_new (FALSE, 0);
	gtk_widget_show (chatData->vbox_whole);
	gtk_container_add (GTK_CONTAINER (chatData->window), chatData->vbox_whole);
	gtk_widget_set_size_request (chatData->vbox_whole, 350, 400);
	
	/*** menu bar *********/
	chatData->menubar = gtk_menu_bar_new ();
	gtk_widget_show (chatData->menubar);
	gtk_box_pack_start (GTK_BOX (chatData->vbox_whole), chatData->menubar, FALSE, FALSE, 0);

	/*** menu item - client ***/
  	chatData->menuitem_cli = gtk_menu_item_new_with_label ("Client");
  	gtk_widget_show (chatData->menuitem_cli);
  	gtk_container_add (GTK_CONTAINER (chatData->menubar), chatData->menuitem_cli);

	/*** menu - client ***/
	chatData->menu_cli = gtk_menu_new ();
	gtk_menu_item_set_submenu (GTK_MENU_ITEM (chatData->menuitem_cli), chatData->menu_cli);
	
	/*** label - connect ***/
	chatData->conn_m = gtk_menu_item_new_with_label ("Connect");
	gtk_widget_show (chatData->conn_m);
	gtk_container_add (GTK_CONTAINER (chatData->menu_cli), chatData->conn_m);

	/*** label - disconnect ***/
	chatData->disconn_m = gtk_menu_item_new_with_label ("Disconnect");
	gtk_widget_show (chatData->disconn_m);
	gtk_container_add (GTK_CONTAINER (chatData->menu_cli), chatData->disconn_m);
	
	/*** separator ***/
	chatData->separatormenuitem_m = gtk_separator_menu_item_new ();
	gtk_widget_show (chatData->separatormenuitem_m);
	gtk_container_add (GTK_CONTAINER (chatData->menu_cli), chatData->separatormenuitem_m);
	gtk_widget_set_sensitive (chatData->separatormenuitem_m, FALSE);

	/*** label - quit ***/
	chatData->quit_m = gtk_menu_item_new_with_label ("Quit");
	gtk_widget_show (chatData->quit_m);
	gtk_container_add (GTK_CONTAINER (chatData->menu_cli), chatData->quit_m);

	/*** menu item - option ***/
	chatData->menuitem_opt = gtk_menu_item_new_with_label ("Option");
	gtk_widget_show (chatData->menuitem_opt);
	gtk_container_add (GTK_CONTAINER (chatData->menubar), chatData->menuitem_opt);

	/*** menu - option ***/
	chatData->menu_opt = gtk_menu_new ();
	gtk_menu_item_set_submenu (GTK_MENU_ITEM (chatData->menuitem_opt), chatData->menu_opt);
	
	/*** label - Load default ***/
	chatData->loadDefu_m = gtk_menu_item_new_with_label ("Load default");
	gtk_widget_show (chatData->loadDefu_m);
	gtk_container_add (GTK_CONTAINER (chatData->menu_opt), chatData->loadDefu_m);

	/*** label - Save as default ***/
	chatData->SaveDefu_m = gtk_menu_item_new_with_label ("Save as default");
	gtk_widget_show (chatData->SaveDefu_m);
	gtk_container_add (GTK_CONTAINER (chatData->menu_opt), chatData->SaveDefu_m);

	/*** label - Preference ***/
	chatData->Prefer_m = gtk_menu_item_new_with_label ("Preference");
	gtk_widget_show (chatData->Prefer_m);
	gtk_container_add (GTK_CONTAINER (chatData->menu_opt), chatData->Prefer_m);

	/*** menu item - View ***/
	chatData->menuitem_view = gtk_menu_item_new_with_label ("View");
	gtk_widget_show (chatData->menuitem_view);
	gtk_container_add (GTK_CONTAINER (chatData->menubar), chatData->menuitem_view);

	/*** menu - View ***/
	chatData->menu_view = gtk_menu_new ();
	gtk_menu_item_set_submenu (GTK_MENU_ITEM (chatData->menuitem_view), chatData->menu_view);
	
	/*** label - Log ***/
	chatData->log_m = gtk_menu_item_new_with_label ("Log");
	gtk_widget_show (chatData->log_m);
	gtk_container_add (GTK_CONTAINER (chatData->menu_view), chatData->log_m);

	/*** menu item - Help ***/
	chatData->menuitem_help = gtk_menu_item_new_with_label ("Help");
	gtk_widget_show (chatData->menuitem_help);
	gtk_container_add (GTK_CONTAINER (chatData->menubar), chatData->menuitem_help);
	
	/*** menu - help ***/
	chatData->menu_help = gtk_menu_new ();
	gtk_menu_item_set_submenu (GTK_MENU_ITEM (chatData->menuitem_help), chatData->menu_help);

	/*** label - About ***/
	chatData->about_m = gtk_menu_item_new_with_label ("About");
	gtk_widget_show (chatData->about_m);
	gtk_container_add (GTK_CONTAINER (chatData->menu_help), chatData->about_m);
	
	/*** hbox for ip and port connection ***/
	chatData->hbox_ipport = gtk_hbox_new (FALSE, 0);
  	gtk_widget_show (chatData->hbox_ipport);
 	gtk_box_pack_start (GTK_BOX (chatData->vbox_whole), chatData->hbox_ipport, FALSE, FALSE, 2);

	/*** ip label ***/
	chatData->ip_lab = gtk_label_new ("IP:");
	gtk_widget_show (chatData->ip_lab);
	gtk_box_pack_start (GTK_BOX (chatData->hbox_ipport), chatData->ip_lab, FALSE, FALSE, 0);

	/*** ip entry ***/
	chatData->ip_ety = gtk_entry_new ();
	gtk_entry_set_text (GTK_ENTRY (chatData->ip_ety), "127.0.0.1");
	gtk_widget_show (chatData->ip_ety);
	gtk_entry_set_max_length (GTK_ENTRY (chatData->ip_ety), MAX_IP_SERVER);
	gtk_box_pack_start (GTK_BOX (chatData->hbox_ipport), chatData->ip_ety, FALSE, TRUE, 0);
	gtk_entry_set_invisible_char (GTK_ENTRY (chatData->ip_ety), 9679);
	gtk_widget_set_size_request (chatData->ip_ety, 180, -1);

	/*** port label ***/
	chatData->port_lab = gtk_label_new ("Port:");
	gtk_widget_show (chatData->port_lab);
	gtk_box_pack_start (GTK_BOX (chatData->hbox_ipport), chatData->port_lab, FALSE, FALSE, 0);

	/*** port entry ***/
	chatData->port_ety = gtk_entry_new ();
	gtk_entry_set_text (GTK_ENTRY (chatData->port_ety), "2145");
	gtk_widget_show (chatData->port_ety);
	gtk_entry_set_max_length (GTK_ENTRY (chatData->port_ety), 5);
	gtk_box_pack_start (GTK_BOX (chatData->hbox_ipport), chatData->port_ety, TRUE, TRUE, 0);
	gtk_entry_set_invisible_char (GTK_ENTRY (chatData->port_ety), 9679);

	/*** connection button ***/
	chatData->conn_btn = gtk_button_new ();
	gtk_widget_show (chatData->conn_btn);
	gtk_box_pack_start (GTK_BOX (chatData->hbox_ipport), chatData->conn_btn, FALSE, FALSE, 0);

	/*** connection icon ***/
	chatData->conn_img = gtk_image_new_from_stock ("gtk-connect", GTK_ICON_SIZE_BUTTON);
	gtk_widget_show (chatData->conn_img);
	gtk_container_add (GTK_CONTAINER (chatData->conn_btn), chatData->conn_img);

	/*** disconnection button ***/
	chatData->disconn_btn = gtk_button_new ();
	gtk_widget_show (chatData->disconn_btn);
	gtk_box_pack_start (GTK_BOX (chatData->hbox_ipport), chatData->disconn_btn, FALSE, FALSE, 0);
	
	/*** disconnection icon ***/
	chatData->disconn_img = gtk_image_new_from_stock ("gtk-disconnect", GTK_ICON_SIZE_BUTTON);
	gtk_widget_show (chatData->disconn_img);
	gtk_container_add (GTK_CONTAINER (chatData->disconn_btn), chatData->disconn_img);
	
	/*** hbox for chat and list's scrolled window and view text ***/
	chatData->hbox_chat = gtk_hbox_new (FALSE, 2);
  	gtk_widget_show (chatData->hbox_chat);
 	gtk_box_pack_start (GTK_BOX (chatData->vbox_whole), chatData->hbox_chat, TRUE, TRUE, 2);

	
	/*** scrolled window for chat output ***/		
	chatData->chat_sw = gtk_scrolled_window_new (NULL, NULL);
	gtk_widget_show (chatData->chat_sw);
	gtk_box_pack_start (GTK_BOX (chatData->hbox_chat), chatData->chat_sw, TRUE, TRUE, 2);
	gtk_widget_set_size_request (chatData->chat_sw, 250, -1);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (chatData->chat_sw), GTK_POLICY_NEVER, GTK_POLICY_ALWAYS);
	gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (chatData->chat_sw), GTK_SHADOW_IN);

	/*** text view - for chat output ***/
	chatData->chat_txv = gtk_text_view_new ();
  	gtk_widget_show (chatData->chat_txv);
  	gtk_container_add (GTK_CONTAINER (chatData->chat_sw), chatData->chat_txv);
  	gtk_text_view_set_wrap_mode (GTK_TEXT_VIEW (chatData->chat_txv), GTK_WRAP_WORD);
	gtk_text_view_set_editable (GTK_TEXT_VIEW (chatData->chat_txv), FALSE);
	gtk_text_view_set_cursor_visible (GTK_TEXT_VIEW (chatData->chat_txv), FALSE);
  	gtk_widget_set_size_request (chatData->chat_txv, 350, -1);
  	
  	/*** scrolled window for online list ***/
  	/*	
	chatData->list_sw = gtk_scrolled_window_new (NULL, NULL);
	gtk_widget_show (chatData->list_sw);
	gtk_box_pack_start (GTK_BOX (chatData->hbox_chat), chatData->list_sw, TRUE, TRUE, 0);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (chatData->list_sw), GTK_POLICY_NEVER, GTK_POLICY_NEVER);
	gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (chatData->list_sw), GTK_SHADOW_IN);
	*/
	/*** text view - for online lis ***/
	/*
	chatData->list_txv = gtk_text_view_new ();
	gtk_widget_show (chatData->list_txv);
	gtk_container_add (GTK_CONTAINER (chatData->list_sw), chatData->list_txv);
	gtk_widget_set_size_request (chatData->list_txv, 100, -1);
	*/
	
	/*** hbox for packing name and online list ***/
	chatData->hbox_namelist = gtk_hbox_new (FALSE, 0);
	gtk_widget_show (chatData->hbox_namelist);
	gtk_box_pack_start (GTK_BOX (chatData->vbox_whole), chatData->hbox_namelist, FALSE, TRUE, 0);

	/*** entry for user name ***/
	chatData->name_lab = gtk_label_new ("Name");
	gtk_widget_show (chatData->name_lab);
	gtk_box_pack_start (GTK_BOX (chatData->hbox_namelist), chatData->name_lab, FALSE, TRUE, 2);

	chatData->name_ety = gtk_entry_new ();
	gtk_widget_show (chatData->name_ety);
	gtk_entry_set_max_length (GTK_ENTRY (chatData->name_ety), MAX_NAME);
	gtk_box_pack_start (GTK_BOX (chatData->hbox_namelist), chatData->name_ety, TRUE, TRUE, 0);
	gtk_entry_set_invisible_char (GTK_ENTRY (chatData->name_ety), 9679);
	gtk_widget_set_size_request (chatData->name_ety, 50, -1);

	/*** ComboBox for onlinelist ***/
	chatData->talkto_lab = gtk_label_new ("TalkTo");
	gtk_widget_show (chatData->talkto_lab);
	gtk_box_pack_start (GTK_BOX (chatData->hbox_namelist), chatData->talkto_lab, FALSE, TRUE, 2);

	chatData->list_cbx = gtk_combo_box_new_text ();
	gtk_widget_show (chatData->list_cbx);
	gtk_box_pack_start (GTK_BOX (chatData->hbox_namelist), chatData->list_cbx, TRUE, TRUE, 0);
	gtk_widget_set_size_request (chatData->list_cbx, 120, -1);
	
	/*** hbox for packing input entry & send button ***/
	chatData->hbox_msgsend = gtk_hbox_new (FALSE, 2);
	gtk_widget_show (chatData->hbox_msgsend);
	gtk_box_pack_start (GTK_BOX (chatData->vbox_whole), chatData->hbox_msgsend, FALSE, TRUE, 2);

	/*** entry text input ***/
	chatData->msg_ety = gtk_entry_new ();
	gtk_widget_show (chatData->msg_ety);
	gtk_widget_set_size_request (chatData->msg_ety, -1, 24);
	gtk_entry_set_max_length (GTK_ENTRY (chatData->msg_ety), 255);
	gtk_entry_set_editable (GTK_ENTRY(chatData->msg_ety), FALSE);
	gtk_box_pack_start (GTK_BOX (chatData->hbox_msgsend), chatData->msg_ety, TRUE, TRUE, 2);
	gtk_entry_set_invisible_char (GTK_ENTRY (chatData->msg_ety), 9679);

	/*** button - send ***/
	chatData->send_btn = gtk_button_new_with_label ("Send");
	gtk_widget_show (chatData->send_btn);
	gtk_box_pack_start (GTK_BOX (chatData->hbox_msgsend), chatData->send_btn, FALSE, FALSE, 2); 

	/*** status bar ***/
	chatData->statusbar = gtk_statusbar_new ();
	gtk_widget_show (chatData->statusbar);
	gtk_box_pack_start (GTK_BOX (chatData->vbox_whole), chatData->statusbar, FALSE, FALSE, 0);

	/*** singals ***/
	g_signal_connect (chatData->window, "destroy", G_CALLBACK (on_window_destroy), chatData);
	g_signal_connect (chatData->msg_ety, "activate", G_CALLBACK (on_send_button_clicked), chatData);
	
	g_signal_connect (chatData->send_btn, "clicked", G_CALLBACK (on_send_button_clicked) ,chatData);
	g_signal_connect (chatData->conn_btn, "clicked", G_CALLBACK (on_connection_button_clicked) ,chatData);
	g_signal_connect (chatData->disconn_btn, "clicked", G_CALLBACK (on_disconnection_button_clicked) ,chatData);
	
	g_signal_connect (chatData->list_cbx, "changed", G_CALLBACK (on_combobox_changed) ,chatData);
	
	g_signal_connect (chatData->conn_m, "activate", G_CALLBACK (on_connection_button_clicked), chatData);
	g_signal_connect (chatData->disconn_m, "activate", G_CALLBACK (on_disconnection_button_clicked), chatData);
	g_signal_connect (chatData->quit_m, "activate", G_CALLBACK (on_window_destroy), chatData);
	g_signal_connect (chatData->about_m, "activate", G_CALLBACK (on_about_menu_item_activate), chatData);
	
	/*** show window ***/
	gtk_widget_show (chatData->window);
	
	/*** show connecting message on status bar ***/
	statusbar_set_message(chatData, "Disconnected");
	
	/*** Connect to Server ***/
	/*
	gint status_id;
	chatData->socket = clientSocket("127.0.0.1",2145);
	status_id = gtk_statusbar_get_context_id((GtkStatusbar*)chatData->statusbar,"Connecting...");
	gtk_statusbar_push ((GtkStatusbar*)chatData->statusbar, status_id, "Connecting...");
	*/		
}

GdkPixbuf *create_pixbuf(const gchar * filename)
{
   GdkPixbuf *pixbuf;
   GError *error = NULL;
   pixbuf = gdk_pixbuf_new_from_file(filename, &error);
   if(!pixbuf) {
      fprintf(stderr, "%s\n", error->message);
      g_error_free(error);
   }
   return pixbuf;
}

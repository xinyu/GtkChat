/********************************
* signal callback definition
*********************************/

#ifndef __CALLBACK_H__
#define __CALLBACK_H__

#include <gtk/gtk.h>

#include "client.h"
#include "definition.h"
#include "../common/common.h"

/*======= signal handler for window ==============*/
void on_window_destroy (GtkObject *object, ChatData* chatData);


/*======= signal handler for buttons ==============*/
void on_send_button_clicked (GtkButton* button, ChatData* chatData);
void on_connection_button_clicked (GtkButton* button, ChatData* chatData);
void on_disconnection_button_clicked (GtkButton* button, ChatData* chatData);

/*======= signal handler for ComboBox ==============*/
void on_combobox_changed (GtkComboBox *combo_box, ChatData* chatData);

/*======= signal handler for g_thread ==============*/
gpointer get_message(gpointer chatData);

/*====== signal handler for menubar =============*/
void on_about_menu_item_activate (GtkMenuItem *menuitem, ChatData* chatData);


void statusbar_set_message (ChatData *chatData, const gchar* message);
void textview_set_message (ChatData *chatData, const gchar* message);

void on_command_activate (ChatData *chatData, char* buffer);

#endif

/******************************************
* Description	: Gtk Based MultiPerson Chat
* Author			: XinYu Lin
* E-mail			: nxforce@yahoo.com
* Date			: 2008.09.01
*******************************************/

#include <gtk/gtk.h>

#include "interface.h"
#include "definition.h"


int main (int argc, char *argv[])
{	
	/*** Create ChatData pointer ***/
	ChatData* chatData;
	
	/*** allocate ChatData struct ***/
	chatData = g_slice_new (ChatData);
	
	/*** set socket to -1 ***/
	chatData->socket = -1;	
	
	/***  Initialize GTK ***/
	gtk_set_locale ();	
	gtk_init (&argc, &argv);	
	
	/*** initialize GUI ***/
	initGUI(chatData);
	
	/*** main() loop ***/
	gtk_main();
	
	/*** free chatData ***/
	g_slice_free (ChatData, chatData);
	
	return 0;
}





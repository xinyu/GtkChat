/****************************************
* struct definition for password generator
*****************************************/

#ifndef __DEFINITION_H__
#define __DEFINITION_H__

typedef struct {
	int socket;
	char name[40];
	
	GtkWidget *window;
	GdkPixbuf *window_icon_pixbuf;
	GtkWidget *vbox_whole;
  
	GtkWidget *menubar;
  
	GtkWidget *menuitem_cli;
	GtkWidget *menu_cli;
	GtkWidget *conn_m;
	GtkWidget *disconn_m;
	GtkWidget *save_m;
	GtkWidget *save_as_m;
	GtkWidget *separatormenuitem_m;
	GtkWidget *quit_m;
  
	GtkWidget *menuitem_opt;
	GtkWidget *menu_opt;
	
	GtkWidget *loadDefu_m;
	GtkWidget *SaveDefu_m;
	GtkWidget *Prefer_m;
   
	GtkWidget *menuitem_view;
	GtkWidget *menu_view;
	GtkWidget *log_m;
  
	GtkWidget *menuitem_help;
	GtkWidget *menu_help;
	GtkWidget *about_m;
  
	GtkWidget *hbox_ipport;
	GtkWidget *ip_lab;
	GtkWidget *ip_ety;
	GtkWidget *port_lab;
	GtkWidget *port_ety;
	GtkWidget *conn_btn;
	GtkWidget *conn_img;
	GtkWidget *disconn_btn;
	GtkWidget *disconn_img;
  
	GtkWidget *hbox_chat;
	GtkWidget *list_sw;
	GtkWidget *list_txv;
	GtkWidget *chat_sw;
	GtkWidget *chat_txv;
  
	GtkWidget *hbox_namelist;
	GtkWidget *name_lab;
	GtkWidget *name_ety;
	GtkWidget *talkto_lab;
	GtkWidget *list_cbx;
  
	GtkWidget *hbox_msgsend;
	GtkWidget *msg_ety;
	GtkWidget *send_btn;
	GtkWidget *statusbar;
  
	GtkAccelGroup *accel_group;
}ChatData;

#endif

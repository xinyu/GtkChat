/*************************************
* Client.c for chat example.
*************************************/

#include "client.h"

int clientSocket(const char* ip, int port)
{

#ifdef _WIN32
	/*** Initial WSA ***/
	int WSAState;
	WSADATA wsaData;
	
	WSAState = WSAStartup(0x101, &wsaData);
	if(WSAState){
		printf("Initial WSA Error!, error code:%d\n", WSAState);
		return -1;
	}
#endif

	/*** Create a Socket ***/
	int ConnectSocket;
	ConnectSocket = socket(PF_INET, SOCK_STREAM, 0);
	if(ConnectSocket == INVALID_SOCKET){
		perror("Error occurred in socket()\n");
	#ifdef _WIN32
		WSACleanup();
	#endif
		return -1;
	}
	
	struct hostent *h;
	unsigned int addr;
	/*** To determine the arguement is host name or ip. ***/
	if(isalpha(ip[0])){
		
		/*** get IP by host name ***/
		h = gethostbyname(ip);
		if(h == NULL){
			printf("Unknown host!\n");
		#ifdef _WIN32
			WSACleanup();
		#endif
			return -1;
		}
		addr = *((unsigned long *)h->h_addr);
	}else{
		addr = inet_addr(ip);
	}
	
	/*********** Inital host address and port *******/
	struct sockaddr_in server;
	
	server.sin_family = AF_INET;
	server.sin_addr.s_addr = addr;
	server.sin_port = htons((u_short)port);
	
	/************ Connect to Server ***************/
	if(connect(ConnectSocket, (struct sockaddr*)&server, sizeof(server)) == SOCKET_ERROR){
		printf("Error occurred in connect()");
	#ifdef _WIN32
		WSACleanup();
	#endif
		return -1;	
	}
	
	printf("#Connecting to Server...!\n");
	return ConnectSocket;
}

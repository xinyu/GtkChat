/***********************************************
* interface definition for Gtk+ MultiPerson Chat
************************************************/

#ifndef __INTERFACE_H__
#define __INTERFACE_H__

#include <gtk/gtk.h>

#include "client.h"
#include "callback.h"
#include "definition.h"
#include "../common/common.h"

void initGUI(ChatData* chatData);
GdkPixbuf* create_pixbuf (const gchar *filename);

#endif

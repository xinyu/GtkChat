/******************************************
* Signal callback for Gtk+ MultiPerson Chat
*******************************************/

#include "callback.h"

#define DEBUG 1

/*======= signal handler for window ==============*/
void on_window_destroy (GtkObject *object, ChatData* chatData)
{
	close(chatData->socket);
	gtk_main_quit();
}

/*======= signal handler for send button ==============*/
void on_send_button_clicked (GtkButton* button, ChatData* chatData)
{
	const gchar *inputBuffer;
	inputBuffer = gtk_entry_get_text(GTK_ENTRY(chatData->msg_ety));
	
	char buffer[MAX_BUF];
	send(chatData->socket, inputBuffer, strlen(inputBuffer), 0);
	
	sprintf(buffer,"%s: %s\n",chatData->name, inputBuffer);
	textview_set_message (chatData, buffer);
	
#if DEBUG == 1
	printf("%s",buffer);
#endif

	/*** to empty entry ***/
	gtk_entry_set_text(GTK_ENTRY(chatData->msg_ety),"");	
}

void on_connection_button_clicked (GtkButton* button, ChatData* chatData)
{
	gboolean isNameEmpty = 0;
	gint port; 
	const gchar* ip;
	const gchar* port_str;
	const gchar* name;
	
	name = gtk_entry_get_text (GTK_ENTRY(chatData->name_ety));
	if(strcmp(name,"") == 0){
		textview_set_message (chatData, "Please enter your name!\n");
		isNameEmpty = 1;
	}
		
	if((chatData->socket == -1) && !isNameEmpty){
		strcpy(chatData->name, name);	
	
		ip = gtk_entry_get_text(GTK_ENTRY(chatData->ip_ety));
		port_str = gtk_entry_get_text(GTK_ENTRY(chatData->port_ety));
		port = atoi(port_str);
		
		chatData->socket = clientSocket(ip, port);
		
		if(chatData->socket == -1){
			char buffer[MAX_BUF];			
			sprintf(buffer,"Fail to connect to %s:%s, maybe this server is offline!\n",ip,port_str);
			textview_set_message(chatData, buffer);
		}else{
			
			gtk_entry_set_editable (GTK_ENTRY(chatData->msg_ety), TRUE);
			send(chatData->socket, chatData->name, strlen(chatData->name), 0);
			/*** create a thread for receiving message ***/
			if(!g_thread_supported()) {
				g_thread_init(NULL);
			}
   
			g_thread_create(get_message, chatData, FALSE, NULL);
		
			/*** show connecting message on status bar ***/
			statusbar_set_message(chatData, "Connecting...");
		}
	}
}

void on_disconnection_button_clicked (GtkButton* button, ChatData* chatData)
{
	int i = 0;
	if(chatData->socket != -1) {
		send(chatData->socket, ":q", 2, 0);
		close(chatData->socket);
		chatData->socket = -1;
		
		for(i=0;i<MAX_ONLINE_USER;i++){
			gtk_combo_box_remove_text (GTK_COMBO_BOX(chatData->list_cbx), 0);					
		}
		
		/*** show connecting message on status bar ***/
		statusbar_set_message(chatData, "Disconnected");
	}
}

/*======= signal handler for ComboBox ==============*/
void on_combobox_changed (GtkComboBox *combo_box, ChatData* chatData)
{
	gint cid;
	char buffer[MAX_BUF];	
	
	cid = gtk_combo_box_get_active(combo_box);
	
	sprintf(buffer,":t %d", cid);
	printf("%s\n",buffer);
	send(chatData->socket, buffer, strlen(buffer), 0);	
}

/*======= signal handler for g_thread ==============*/
gpointer get_message(gpointer chatData)
{
	char socketBuffer[MAX_BUF];
	char buffer[MAX_BUF];
	
	int ret, maxfd = -1;
	fd_set rfds;

	while (1){
		/*** initiaize FD ****/
		FD_ZERO(&rfds);
		maxfd = ((ChatData*)chatData)->socket;
		FD_SET(((ChatData*)chatData)->socket, &rfds);

		/*** select and wait ***/
		ret = select(maxfd + 1, &rfds, NULL, NULL, NULL);
	
		if(ret == -1){
			printf("Select() Error: %s", strerror(errno));
			break;
		}else if(ret == 0){
			continue;
		}else{
			
			/*** check socket side's message ***/
			if(FD_ISSET(((ChatData*)chatData)->socket, &rfds)){
				bzero(socketBuffer, MAX_BUF);
				ret = recv(((ChatData*)chatData)->socket, socketBuffer, MAX_BUF, 0);
				if(ret > 0){
					socketBuffer[ret]='\0';
					if(socketBuffer[0]==':'){
						on_command_activate(chatData, socketBuffer);
					}else{
						bzero(buffer, MAX_BUF);
						sprintf(buffer,"%s\n",socketBuffer);
						gdk_threads_enter ();
						textview_set_message(chatData, buffer);
						gdk_threads_leave ();
					}
#if DEBUG == 1
					printf("%s",buffer);
#endif
				}else{
					if (ret < 0){
						printf("Code:%d, fail to receive message from server.'%s'\n", errno, strerror(errno));
					}else if(ret == 0){
						textview_set_message (chatData,"Server is offline!\n");
						gtk_signal_emit_by_name ((GtkObject*)(((ChatData*)chatData)->disconn_btn),"clicked");
						break;
					}	
				}
			}		
		}
	}	
	return NULL;
}


/*====== signal handler for menubar =============*/
void on_about_menu_item_activate (GtkMenuItem *menuitem, ChatData* chatData)
{
	static const gchar * const authors[] = {"Xin-Yu Lin <nxforce@yahoo.com>", NULL};

	static const gchar copyright[] = "Copyright \xc2\xa9 2008 Xin-Yu Lin";

	static const gchar comments[] = "GTK+2.0 Programming";

	gtk_show_about_dialog (GTK_WINDOW (chatData->window),
			       "authors", authors,
			       "comments", comments,
			       "copyright", copyright,
			       "version", "0.1",
			       "website", "http://nxforce.blogspot.com",
			       "program-name", "GTK+ MultiPerson Chat",
			       "logo-icon-name", GTK_STOCK_EDIT,
			       NULL); 
}

/*==========  self-defined functions ===========*/
void statusbar_set_message (ChatData *chatData, const gchar* message)
{
	gint status_id;
	status_id = gtk_statusbar_get_context_id((GtkStatusbar*)chatData->statusbar, message);
	gtk_statusbar_push ((GtkStatusbar*)chatData->statusbar, status_id, message);
}

void textview_set_message (ChatData *chatData, const gchar* message)
{
	GtkTextIter iter;
	GtkTextBuffer *textBuffer = NULL;
	GtkTextMark* textMark;
	
	textBuffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(chatData->chat_txv));
	gtk_text_buffer_get_end_iter(textBuffer, &iter);
	gtk_text_buffer_insert(textBuffer, &iter, message, strlen(message));
	gtk_text_buffer_get_end_iter(textBuffer, &iter);
	
	textMark = gtk_text_buffer_get_insert (textBuffer);
  	gtk_text_buffer_place_cursor( textBuffer, &iter);
	gtk_text_view_scroll_to_mark( GTK_TEXT_VIEW (chatData->chat_txv), textMark, 0.0, TRUE, 0.0, 0.0); 
}

/*========== command prasing ===========*/
void on_command_activate (ChatData *chatData, char* buffer)
{
	int cid, num = 0, count = 0;
	char* ptr , *temp;
	char name[MAX_NAME];
	
	printf("%s\n",buffer);
	switch(buffer[1]){
		case 't':
			switch(buffer[2]){
				case '0':
					ptr = &buffer[4];
					cid = atoi(ptr);
			
					while(*ptr++!=' ');
					gtk_combo_box_remove_text (GTK_COMBO_BOX(chatData->list_cbx), cid);
					break;
				case '1':
					ptr = &buffer[4];
					cid = atoi(ptr);

					while(*ptr++!=' ');
					gtk_combo_box_remove_text (GTK_COMBO_BOX(chatData->list_cbx), cid);
					gtk_combo_box_insert_text (GTK_COMBO_BOX(chatData->list_cbx), cid, ptr);					
					
					break;				
				case '2':
					ptr = &buffer[4];
					cid = atoi(ptr);
					
					gtk_combo_box_set_active (GTK_COMBO_BOX(chatData->list_cbx), cid);	
					
					break;				
				case 'n':
					ptr = &buffer[4];
					num = atoi(ptr);
					if(num){
						/*** jump to ':' character ***/
						while(*ptr!=':') ptr++;	
						
						/*** for large online user purpose, but this function is still incomplete ***/
						while(count<num){
							
							/*** skip the ':' character ***/
							ptr++;
							
							/*** copy cid ***/
							cid = atoi(ptr);
							
							/*** jump to ' ' character ***/						
							while(*ptr++!=' ');
							
							/*** record the address of the first character of name ***/
							temp = ptr;
							
							/*** jump to ':'  character ***/
							while(*ptr!=':' && *ptr)	ptr++;						
							
							/*** copy name ***/
							memset(name,'\0',MAX_NAME);
							strncpy(name, temp, ptr-temp);
							name[ptr-temp]='\0';
							
							/*** add new user on ComboBox ***/ 
							gtk_combo_box_insert_text (GTK_COMBO_BOX(chatData->list_cbx), cid, name);
							count++;
						}
					}
					break;
				default:
					printf("Unknown parameter for update ComboBox\n");					
					break;
			}					
			break;
		default:
			printf("Invaild Command!\n");
			break;
	}
}

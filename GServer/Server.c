/***********************************************************************
***************	Server.c  
* Description	: Server.c for Gtk+ multiperson chat example.
*
* Compiler		: GCC 4.2.3
* Date			: 2008.08.26
* Author			: Xin Yu Lin
* E-mail			: nxforce@yahoo.com
* Blog			: nxforce.blogspot.com	
* Reference		: Beej��s Guide to Network ProgrammingUsing Internet Sockets
*				     Brian ��Beej�� Hall, beej@beej.us				***********************************************************************/

#include "Server.h"

int main(int argc, char* argv[])
{

#ifdef _WIN32
	/*** Initial WSA ***/
	int WSAState;
	WSADATA wsaData;
	
	WSAState = WSAStartup(0x101, &wsaData);
	if(WSAState){
		printf("Initial WSA Error!, error code:%d\n", WSAState);
		exit(EXIT_FAILURE);
	}
#endif

	u_short port;
	
	switch(argc){
		case 2:
			port = (u_short)atoi(argv[1]);
			break;
		default:
			port = 2145;
			break;
	}
	
	/*** Create a Socket ***/
	int Server;
	Server = socket(PF_INET,SOCK_STREAM,0);
	if(Server == INVALID_SOCKET){
		printf("Error occurred in socket()");
	#ifdef _WIN32
		WSACleanup();
	#endif
		exit(EXIT_FAILURE);
	}
	
	/*** set re-bind Server immediately ***/
	int reuse_addr = 1;
	setsockopt(Server, SOL_SOCKET, SO_REUSEADDR, &reuse_addr,sizeof(reuse_addr));
	
	/*** Initial host address and port ***/
	struct sockaddr_in local;
	
	local.sin_family = AF_INET;
	local.sin_addr.s_addr = INADDR_ANY;
	local.sin_port = htons(port);
	
	/*** bind Socket ***/
	if(bind(Server, (struct sockaddr*)&local,sizeof(local)) == SOCKET_ERROR){
		printf("Error occurred in bind()");
	#ifdef _WIN32
		WSACleanup();
		closesocket(Server);
	#else
		close(Server);
	#endif
		exit(EXIT_FAILURE);
	}
	
	/*** Create socket listener ***/
	if(listen(Server,1) == SOCKET_ERROR){
		printf("Error occurred in listen()");
	#ifdef _WIN32
		WSACleanup();
		closesocket(Server);
	#else
		close(Server);
	#endif
		exit(EXIT_FAILURE);
	}
	
	/***************** Create a SOCKET for accepting incoming ***********/
	int i, sockno;
	
	int fdmax;					/* maximum fd number */
	int ret;						/* to save return number */
	int uid;						/* user id */
	int tid;						/* talker id */
	int NumOfOnline = 0;			/* online user counter */
	int AcceptSocket; 		/* client socket */
	int shutdown = 0;			/* for shutdown flag */
	
	/* for getting more user information */
	struct sockaddr_in sa_client;
	socklen_t addr_len = sizeof(sa_client);
	
	/* send/receive buffer */
	char buffer[MAX_BUF], bufTemp[MAX_BUF];
	
	/* FD sets */
	fd_set read_fds;				/* update by master */	
	fd_set master;					/* update by new connection or disconnection */
		
	FD_ZERO(&master);				/* to empty master set */		
	FD_ZERO(&read_fds);			/* to empty read_fds	set */ 
	FD_SET(0, &master);			/* add StdinFD into master set */
	FD_SET(Server, &master);	/* add Server into master set */
	fdmax = Server;					/* set Server as maximum fd number */	
		
  	printf("%s#Waiting for client to connect...%s\n",BYELLOW,WHITE);
	
	/* allocate and initialize new onlineList */
	struct OnlineList* onlineList=(struct OnlineList*)calloc(MAX_ONLINE_USER,sizeof(struct OnlineList));	
	for(i=0; i < MAX_ONLINE_USER; i++) onlineList[i].isOnline=0;
	
	do{
		/* copy master to read_fds */
		read_fds = master;
		
		if(select(fdmax+1, &read_fds, NULL, NULL, NULL) == -1){
			printf("Select() Error: %s\n", strerror(errno));
			exit(EXIT_FAILURE);
		}	
		
		/* scan every exist connection */
		for(sockno = 0; sockno <= fdmax; sockno++) {
			
			if (FD_ISSET(sockno, &read_fds)) {
			
				/*========== for server's stardard input =========*/
				if(sockno == 0){ 
					fgets(buffer, MAX_BUF, stdin);
					
					/*** Server want to quit! ***/
					if(!strncmp(buffer, ":q", 2)){
						bzero(buffer, MAX_BUF );
						sprintf(buffer,"Server is going to offline!");
						for(i = 0; i < MAX_ONLINE_USER; i++){
							if(onlineList[i].isOnline){
								send(onlineList[i].socket, buffer,strlen(buffer),0);
							#ifdef _WIN32
								closesocket(onlineList[i].socket);
							#else
								close(onlineList[i].socket);
							#endif							
							}						
						}
						
						shutdown = 1;
					}
				/*========= for new user ========================*/	
				}else if (sockno == Server) {
					if ((AcceptSocket = accept( Server, (struct sockaddr *)&sa_client, &addr_len )) == SOCKET_ERROR) {
						perror("Error occurred in accept()\n");
					}else{
						printf("Socket(%d) Connection from %s:%d\n",AcceptSocket , inet_ntoa(sa_client.sin_addr),sa_client.sin_port);
						
						/*** search empty space to store new user ***/
						uid = addUser(onlineList, AcceptSocket, NumOfOnline);
												
						if( uid != -1 ){
							/*** increase total online number ***/
							NumOfOnline++;
													
							/*** send login message to this new guy ****/
												
							bzero(buffer, MAX_BUF );					
							sprintf(buffer,"=== Welcome to MultiPerson Chat Room ===");
							send(AcceptSocket, buffer, MAX_BUF, 0);
							
							
							/*** get user name ***/
							bzero(buffer, MAX_BUF );
							ret = recv(AcceptSocket, buffer, MAX_BUF, 0);
							strncpy(onlineList[uid].name, buffer, MAX_NAME);
							
							printf("%s is online\n",onlineList[uid].name);							
							
							/*** add user's socket into FD set ***/
							FD_SET(AcceptSocket, &master);
							if (AcceptSocket > fdmax) {
								fdmax = AcceptSocket;
							}
							
							/*** tell everyone this user is online and update their onlinelist ***/
							bzero(buffer, MAX_BUF );
							sprintf(buffer,":t1 %d %s",onlineList[uid].cid, onlineList[uid].name);
							for(i = 0; i < MAX_ONLINE_USER; i++){
								if(onlineList[i].isOnline && i != uid){
									send(onlineList[i].socket, buffer, strlen(buffer),0);						
								}						
							}
							
							/*** send whole onlinelsit to new user ***/
							bzero(buffer, MAX_BUF );
							sprintf(buffer,":tn %d", NumOfOnline);
							
							for(i = 0; i < MAX_ONLINE_USER; i++){
								if(onlineList[i].isOnline){
									bzero(bufTemp, MAX_BUF );
									sprintf(bufTemp,":%d %s", onlineList[i].cid, onlineList[i].name);
									strcat(buffer,bufTemp);						
								}						
							}
							strcat(buffer,":");	
							send(onlineList[uid].socket, buffer, strlen(buffer),0);
												
						}else{
						
							/*** Sorry! Server is full ***/
							bzero(buffer, MAX_BUF );
							sprintf(buffer,"%s","Sorry! server is full, please connect it later!");
							send(AcceptSocket, buffer, strlen(buffer), 0);
							close(AcceptSocket);
						#ifdef _WIN32
							closesocket(AcceptSocket);
						#else
							close(AcceptSocket);
						#endif
						}
					}
					
				/*========= for normal chating ========================*/		
				} else {
				
					/*** Can't hear what user says */
					if ((ret = recv(sockno, buffer, MAX_BUF, 0)) <= 0) {
						/* It means that this user is offline */
						if (ret < 0) {
							printf("%sCode:%d, Fail to receive message from Scoket(%d)! %s%s\n", BGREEN, errno, sockno, strerror(errno), WHITE);
						}
						
						uid = findUser_by_socket(onlineList, sockno);
						if(uid != -1){
							onUserOffline(onlineList, uid, &master, &NumOfOnline);
						}else{
							printf("%sCan't find uid for setting offline!%s\n", BGREEN, WHITE);
						}
						
					} else {
						buffer[ret]='\0';
						
						uid = findUser_by_socket(onlineList, sockno);
						if(uid != -1){
						
							/* check reserved command character */
							if(buffer[0]==':'){
								cmd(onlineList, buffer, uid ,&master, &NumOfOnline);
							}else{
								
								/*** get talker's id ***/
								tid = onlineList[uid].tid;								
								
								/* this user hasn't told me who is his/her talker? */			
								if(tid == -1){
									bzero(buffer, MAX_BUF );
									sprintf(buffer,"Server: who would you want to talking to ?");
									send(sockno, buffer, strlen(buffer), 0);
								}else{
									
									/* user's talker is alone. */
									/*									
									if(onlineList[tid].tid == -1){ 
										onlineList[tid].tid = uid;
										bzero(buffer, MAX_BUF );
										sprintf(bufTemp,":t2 %d", uid);
										send(onlineList[tid].socket, bufTemp, MAX_BUF, 0);
									}else */
										
									/* user's talker is not talk to user. */									
									if(onlineList[tid].tid != uid){
									 	bzero(bufTemp, MAX_BUF );
										send(sockno, bufTemp, strlen(bufTemp), 0);
									}
									
									/* user still send this messages to the talker */
									bzero(bufTemp, MAX_BUF );
									sprintf(bufTemp,"%s: %s", onlineList[uid].name, buffer);
									ret = send(onlineList[tid].socket, bufTemp , strlen(bufTemp) , 0);
									
									if(ret < 0){
										/* you are so unlucky */
										bzero(buffer, MAX_BUF );
										sprintf(bufTemp,"Server: Fail to send the message %s to %s, maybe %s is offline!", buffer, onlineList[tid].name, onlineList[tid].name);
										send(sockno, bufTemp, strlen(bufTemp), 0);
									}
								}
							}
						}else{
							printf("%sSocket(%d) is not on the online list! What's wrong with our program?%s\n", BGREEN, sockno, WHITE);						
						}	/* if((uid = findUser... */
					}	/* if ((ret = recv( .... */
				}	/* if(sockno == 0)... */
			}	/* if (FD_ISSET(sockno, ...  */	
		}	/* for sockno ... to ... fdmax  */
	}while(!shutdown);
	
	free(onlineList);
	
	/*** close server socket ***/
#ifdef _WIN32
	WSACleanup();
	closesocket(Server);
#else
	close(Server);
#endif
	return 0;
}

/*** check user command ***/
int cmd(struct OnlineList* onlineList, char* cmd, int uid, fd_set* master, int* NumOfOnline)
{
	int tid, cid;
	char* ptr;
	char buffer[MAX_BUF];

	switch(cmd[1]){
		case 'n':
			ptr = &cmd[3];
			strncpy(onlineList[uid].name, ptr, MAX_NAME);
			return 1;
		case 't':
			ptr = &cmd[3];
			cid = atoi(ptr);		
			
			tid = findUser_by_cid(onlineList, cid);
			if(tid !=  -1){
				onlineList[uid].tid = tid;
			}else{
				bzero(buffer, MAX_BUF );
				sprintf(buffer,"Server: your talker is  offline");
				send(onlineList[uid].socket, buffer, strlen(buffer), 0);
			}			
			return 1;
		case 'q':
			onUserOffline(onlineList, uid, master, NumOfOnline);
			return 0;
		default:
			sprintf(buffer,"Server: Unknown Command!");
			send(onlineList[uid].socket, buffer, strlen(buffer), 0);
			return -1;		 	
	}
}

/*** add user, return -1 if server is full ***/ 
int addUser(struct OnlineList* onlineList, int socket, int cid)
{
	int i;
	for(i=0;i<MAX_ONLINE_USER;i++){
		if(!onlineList[i].isOnline){
			onlineList[i].socket = socket;
			onlineList[i].cid = cid;
			onlineList[i].tid = -1;
			onlineList[i].isOnline = 1;
			return i;
		}	
	}
	return -1;
}

/*** find user id by scoket number, return -1 if failed */
int findUser_by_socket(struct OnlineList* onlineList, int socket)
{
	int i;
	for(i=0;i<MAX_ONLINE_USER;i++){
		if(onlineList[i].socket == socket) return i;	
	}
	return -1;
}

/*** find user id by ComboBox id, return -1 if failed ***/
int findUser_by_cid (struct OnlineList* onlineList, int cid)
{
	int i;
	for(i=0;i<MAX_ONLINE_USER;i++){
		if(onlineList[i].cid == cid) return i;	
	}
	return -1;
}

/*** for user offline ***/
void onUserOffline (struct OnlineList* onlineList, int uid, fd_set* master, int* NumOfOnline)
{
	int i;
	char buffer[MAX_BUF];
	
	printf("Socket(%d):%s is offline\n", onlineList[uid].socket, onlineList[uid].name);
	printf("%sTotoal online:%d%s\n",GREEN, --*NumOfOnline, WHITE);
	
	/*** talk to all the user's friend, this user is already offline ***/
	bzero(buffer, MAX_BUF );
	//sprintf(buffer,"Server: %s is offline", onlineList[uid].name);
	sprintf(buffer,":t0 %d", onlineList[uid].cid);
	for(i = 0; i < MAX_ONLINE_USER ;i++ ){
		if(onlineList[i].tid == uid ){
			send(onlineList[i].socket, buffer, strlen(buffer), 0);
			onlineList[i].tid = -1;
		}
	}
	
	/* close socket and remove it from fd set */
	FD_CLR(onlineList[uid].socket, master);
	
#ifdef _WIN32
	closesocket(onlineList[uid].socket);
#else
	close(onlineList[uid].socket);
#endif
		
	/*** erase the record ***/
	onlineList[uid].isOnline = 0;
	onlineList[uid].socket = -1;
	onlineList[uid].cid = -1;
	onlineList[uid].tid = -1;
	memset(onlineList[uid].name, '\0' ,MAX_NAME);		
}


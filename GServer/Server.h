/***********************************************************************
***************	Server.h   
* Description	: definition for server.c 
*
* Compiler		: GCC 4.2.3
* Date			: 2008.08.26
* Author		: Xin Yu Lin
* E-mail		: nxforce@yahoo.com
* Blog			: nxforce.blogspot.com	
* Reference	: Beej��s Guide to Network ProgrammingUsing Internet Sockets
*				     Brian ��Beej�� Hall, beej@beej.us				***********************************************************************/

#ifndef _SERVER_H_
#define _SERVER_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>

#ifdef _WIN32
	#include <winsock.h>
#else
	#define SOCKET int
	#define INVALID_SOCKET -1
	#define SOCKET_ERROR -1
	
	#include <netdb.h>
	#include <sys/socket.h>
	#include <netinet/in.h>
	#include <arpa/inet.h>
	#include <unistd.h>
#endif

#include "setcolor.h"
#include "../common/common.h"


struct OnlineList{
	int socket;						/*** to stroe user's socket ***/
	char name[MAX_NAME];	/*** user name	***/
	int cid;							/*** index of user's GtkComboBox ***/
	int tid;							/*** talker's id ***/
	int isOnline;					/*** flag for online ***/
};

int addUser(struct OnlineList* onlineList, int socket, int cid);
int findUser_by_socket(struct OnlineList* onlineList, int socket);
int findUser_by_cid (struct OnlineList* onlineList, int tid);
int cmd(struct OnlineList* onlineList, char* cmd, int uid, fd_set* master, int* NumOfOnline);
void chat(int socket);
void onUserOffline (struct OnlineList* onlineList, int uid, fd_set* master, int* NumOfOnline);

#endif

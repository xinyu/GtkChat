/****************************************
* struct definition for password generator
*****************************************/

#ifndef __COMMON_H__
#define __COMMON_H__

#define MAX_ONLINE_USER 10
#define MAX_IP_SERVER 80
#define MAX_NAME 40
#define MAX_BUF 2048

#endif
